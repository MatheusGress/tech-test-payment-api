using System.Collections.Generic;
using System.Linq;
using SalesApi.Models;

namespace SalesApi.Services
{
    public class SalesService
    {
        private readonly List<Sale> _sales = new List<Sale>();
        private int _nextId = 1;

        public Sale RegisterSale(Sale sale)
        {
            sale.Id = _nextId++;
            sale.Status = "Aguardando pagamento";
            _sales.Add(sale);
            return sale;
        }

        public Sale GetSaleById(int id)
        {
            return _sales.FirstOrDefault(s => s.Id == id);
        }

        public Sale UpdateSaleStatus(int id, string status)
        {
            var sale = GetSaleById(id);
            if (sale != null)
            {
                if (IsValidStatusTransition(sale.Status, status))
                {
                    sale.Status = status;
                }
            }
            return sale;
        }

        private bool IsValidStatusTransition(string currentStatus, string newStatus)
        {
            var validTransitions = new Dictionary<string, List<string>>
            {
                { "Aguardando pagamento", new List<string> { "Pagamento aprovado", "Cancelada" } },
                { "Pagamento aprovado", new List<string> { "Enviado para Transportadora", "Cancelada" } },
                { "Enviado para Transportadora", new List<string> { "Entregue" } }
            };

            return validTransitions.ContainsKey(currentStatus) && validTransitions[currentStatus].Contains(newStatus);
        }
    }
}
