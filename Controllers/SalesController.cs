using Microsoft.AspNetCore.Mvc;
using SalesApi.Models;
using SalesApi.Services;

namespace SalesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private readonly SalesService _salesService;

        public SalesController(SalesService salesService)
        {
            _salesService = salesService;
        }

        [HttpPost]
        public ActionResult<Sale> RegisterSale(Sale sale)
        {
            if (sale.Items == null || sale.Items.Count == 0)
            {
                return BadRequest("A sale must contain at least one item.");
            }
            var registeredSale = _salesService.RegisterSale(sale);
            return CreatedAtAction(nameof(GetSaleById), new { id = registeredSale.Id }, registeredSale);
        }

        [HttpGet("{id}")]
        public ActionResult<Sale> GetSaleById(int id)
        {
            var sale = _salesService.GetSaleById(id);
            if (sale == null)
            {
                return NotFound();
            }
            return sale;
        }

        [HttpPut("{id}")]
        public ActionResult<Sale> UpdateSaleStatus(int id, [FromBody] string status)
        {
            var sale = _salesService.UpdateSaleStatus(id, status);
            if (sale == null)
            {
                return NotFound();
            }
            return sale;
        }
    }
}
