namespace SalesApi.Models
{
    public class Sale
    {
        public int Id { get; set; }
        public Seller Seller { get; set; }
        public DateTime Date { get; set; }
        public List<string> Items { get; set; }
        public string Status { get; set; }
    }
}
